# My own OpenCV Docker (Nvidia)
CUDAをサポートしたOpenCVを使うためのDocker環境

CPU版は **cpuブランチ**をお使いください。

# Docker イメージ
FROM イメージ：　nvidia/cudagl:11.3.1-devel-ubuntu20.04<br>
CUDA : 11.3.1<br>
Ubuntu ： 20.04<br>
OpenCV : 4.5.2<br>
ROS : Noetic<br>

## Ubuntu20.04にOpenCV4.5.2をインストール
基本環境<br>
[Docker ユーザ名]/opencv:cudagl<br>

開発環境（エディタ等）<br>
[Docker ユーザ名]/opencv-user:cudagl

## Ubuntu20.04にROS noetic, OpenCV4.5.2をインストール
基本環境<br>
[Docker ユーザ名]/opencv-ros:cudagl<br>

開発環境（エディタ等）<br>
[Docker ユーザ名]/opencv-ros-user:cudagl<br>


# インストール
下記の通り、リポジトリをクローンします。
```
$ cd ~
$ git clone --branch nvidia https://gitlab.com/okadalaboratory/opencv/my-own-opencv.git opencv-gpu
$ cd opencv-gpu
```
下記の通り、Dockerイメージを作成します。初回はしばらく時間がかかるので気長にお待ちください。
```
$ make build-all
```
# イメージ名
Dockerイメージは下記のようになっています<br>

**[Docker ユーザ名]/[イメージ名]:[タグ名]**

[Docker ユーザ名]はMakefileで指定しているので、Dockerイメージのビルドの前に、お好みで変更してください。


# Makefile
Dockerイメージの作成やコンテナの実行はmakeコマンドで行います。

下記の通り　make help コマンドで確認することができます。
```
$ make help
help             This help.
build-base       Build Ubuntu20.04 OpenCV [GPU] Base Image
build-user       Build Ubuntu20.04 OpenCV [GPU] User Image
build-ros        Build Ubuntu20.04 OpenCV [GPU] ROS Image
build-ros-user   Build Ubuntu20.04 OpenCV [GPU] ROS User Image
run　　             RUN Ubuntu20.04 OpenCV [GPU] Base Container
run-user         RUN Ubuntu OpenCV [GPU] User Container
run-ros          RUN Ubuntu20.04 OpenCV [GPU] ROS Container
run-ros-user     RUN Ubuntu OpenCV [GPU] ROS User Container

```
#　動作確認
```
$ make run
```
Dockerコンテナを起動し、以下のコマンドを実行し0より大きい値が返ってくれば成功
```
$ python3
import cv2
print(cv2.cuda.getCudaEnabledDeviceCount())
4
```
