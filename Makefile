# Copyright (c) 2022 Hiroyuki Okada
# This software is released under the MIT License.
# http://opensource.org/licenses/mit-license.php

UNAME:=$(shell id -un)
GNAME:=$(shell id -gn)
UID:=$(shell id -u)
GID:=$(shell id -g) 

DOCKER_USER = okdhryk/
DOCKER_DEVICES = --device="/dev/dri:/dev/dri"
DOCKER_VOLUMES = --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw"  --volume="$(HOME)/.Xauthority:$(HOME)/.Xauthority"   --volume="$(PWD)/share:$(HOME)/share" 
DOCKER_ENV_VARS = --env="DISPLAY=$(DISPLAY)" --env="XAUTHORITY=$(HOME)/.Xauthority" 
DOCKER_ENV_USER = --env="USER_NAME=$(UNAME)" --env="USER_ID=$(UID)" --env="GROUP_NAME=$(GNAME)" --env="GROUP_ID=$(GID)" 

# If the first argument is ...
ifneq (,$(findstring tools_,$(firstword $(MAKECMDGOALS))))
	# use the rest as arguments
	RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
	# ...and turn them into do-nothing targets
	#$(eval $(RUN_ARGS):;@:)
endif

.PHONY: help

help: ## This help.
	@awk 'BEGIN {FS = ":.*?## "} /^[\.0-9a-zA-Z_-]+:.*?## / {printf "\033[36m%-42s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help

build-all: build-base build-user build-ros build-ros-user
	@printf "\n\033[92mBuild Ubuntu20.04 OpenCV [GPU] All Images\033[0m\n"

build-base: ## Build Ubuntu20.04 OpenCV [GPU] Base Image
	docker build --target base -t $(DOCKER_USER)opencv:cudagl -f docker/Dockerfile .
	@printf "\n\033[92mBuild Docker Image: $(DOCKER_USER)opencv:cudagl\033[0m\n"

build-user: ## Build Ubuntu20.04 OpenCV [GPU] User Image
	docker build --target user -t $(DOCKER_USER)opencv-user:cudagl -f docker/Dockerfile .
	@printf "\n\033[92mBuild Docker Image: $(DOCKER_USER)opencv-user:cudagl\033[0m\n"

build-ros: ## Build Ubuntu20.04 OpenCV [GPU] ROS Image
	docker build --target ros -t $(DOCKER_USER)opencv-ros:cudagl -f docker/Dockerfile .
	@printf "\n\033[92mBuild Docker Image: $(DOCKER_USER)opencv-ros:cudagl\033[0m\n"

build-ros-user: ## Build Ubuntu20.04 OpenCV [GPU] ROS User Image
	docker build --target ros-user -t $(DOCKER_USER)opencv-ros-user:cudagl -f docker/Dockerfile .
	@printf "\n\033[92mBuild Docker Image: $(DOCKER_USER)opencv-ros-user:cudagl\033[0m\n"


run: ## RUN Ubuntu20.04 OpenCV [GPU] Base Container
	docker run --rm -it --gpus all --privileged --net=host --ipc=host \
	$(DOCKER_DEVICES) $(DOCKER_VOLUMES) $(DOCKER_ENV_VARS) $(DOCKER_ENV_USER) \
	-v /var/run/dbus:/var/run/dbus -v /var/run/avahi-daemon/socket:/var/run/avahi-daemon/socket \
	$(DOCKER_USER)opencv:cudagl
	@printf "\n\033[92mRun Docker Image: $(DOCKER_USER)opencv:cudagl\033[0m\n"

run-user: ## RUN Ubuntu OpenCV [GPU] User Container
	docker run --rm -it --gpus all --privileged --net=host --ipc=host \
	$(DOCKER_DEVICES) $(DOCKER_VOLUMES) $(DOCKER_ENV_VARS) $(DOCKER_ENV_USER) \
	-v /var/run/dbus:/var/run/dbus -v /var/run/avahi-daemon/socket:/var/run/avahi-daemon/socket \
	$(DOCKER_USER)opencv-user:cudagl
	@printf "\n\033[92mRun Docker Image: $(DOCKER_USER)opencv-uer:cudagl\033[0m\n"

run-ros: ## RUN Ubuntu20.04 OpenCV [GPU] ROS Container
	docker run --rm -it --gpus all --privileged --net=host --ipc=host \
	$(DOCKER_DEVICES) $(DOCKER_VOLUMES) $(DOCKER_ENV_VARS) $(DOCKER_ENV_USER) \
	-v /var/run/dbus:/var/run/dbus -v /var/run/avahi-daemon/socket:/var/run/avahi-daemon/socket \
	$(DOCKER_USER)opencv-ros:cudagl
	@printf "\n\033[92mRun Docker Image: $(DOCKER_USER)opencv-ros:cudagl\033[0m\n"

run-ros-user: ## RUN Ubuntu OpenCV [GPU] ROS User Container
	docker run --rm -it --gpus all --privileged --net=host --ipc=host \
	$(DOCKER_DEVICES) $(DOCKER_VOLUMES) $(DOCKER_ENV_VARS) $(DOCKER_ENV_USER) \
	-v /var/run/dbus:/var/run/dbus -v /var/run/avahi-daemon/socket:/var/run/avahi-daemon/socket \
	$(DOCKER_USER)opencv-ros-user:cudagl
	@printf "\n\033[92mRun Docker Image: $(DOCKER_USER)opencv-ros-user:cudagl\033[0m\n"
