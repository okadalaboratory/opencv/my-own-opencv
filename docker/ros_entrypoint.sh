#!/bin/bash
set -e
#! /bin/bash
# Copyright (c) 2022 Hiroyuki Okada
# This software is released under the MIT License.
# http://opensource.org/licenses/mit-license.php

set -e
DEFAULT_USER=${DEFAULT_USER:-'ubuntu'}
DEFAULT_USER_UID=${USER_UID:-'1000'}
DEFAULT_USER_GID=${USER_GID:-'1000'}
NOPASSWD=${NOPASSWD:-''} # set 'NOPASSWD:' to disable asking sudo password

# docker run を実行しているユーザが root なのか、カレントユーザなのか？
check_users(){
    CURRENT_USER_OK=true;
    if [ -z ${USER_NAME+x} ]; then 
        CURRENT_USER_OK=false;
        return;
    fi
    if [ -z ${USER_ID+x} ]; then 
        CURRENT_USER_OK=false;
        return;
    else
        if ! [ -z "${USER_ID##[0-9]*}" ]; then 
            echo -e "\033[1;33mWarning: User-ID should be a number. Falling back to defaults.\033[0m"
            CURRENT_USER_OK=false;
            return;
        fi
    fi
    if [ -z ${GROUP_NAME+x} ]; then 
        CURRENT_USER_OK=false;
        return;
    fi

    if [ -z ${GROUP_ID+x} ]; then 
        CURRENT_USER_OK=false;
        return;
    else
        if ! [ -z "${GROUP_ID##[0-9]*}" ]; then 
            echo -e "\033[1;33mWarning: Group-ID should be a number. Falling back to defaults.\033[0m"
            CURRENT_USER_OK=false;
            return;
        fi
    fi
}

setup_user () {
    USER=$1
    USER_ID=$2
    GROUP=$3
    GROUP_ID=$4

    ## Create user
    useradd -m $USER -G sudo,video

    ## Copy bash/sh configs
    cp /root/.profile /home/$USER/
    cp /root/.bashrc /home/$USER/

    ## Copy terminator configs
    mkdir -p /home/$USER/.config/terminator
    cp /root/.config/terminator/config /home/$USER/.config/terminator/config

    # Copy SSH keys & fix owner
    if [ -d "/root/.ssh" ]; then
        cp -rf /root/.ssh /home/$USER/
        chown -R $USER_ID:$GROUP_ID /home/$USER/.ssh
    fi

    ## Fix owner
    if [ -d "/overlay_ws" ]; then
        chown -R $USER_ID:$GROUP_ID /overlay_ws
    fi
    chown $USER_ID:$GROUP_ID /home/$USER
    chown -R $USER_ID:$GROUP_ID /home/$USER/share
    chown -R $USER_ID:$GROUP_ID /home/$USER/.config
    chown $USER_ID:$GROUP_ID /home/$USER/.profile
    chown $USER_ID:$GROUP_ID /home/$USER/.bashrc

    ## This a trick to keep the evnironmental variables of root which is important!
    echo "if ! [ \"$USER\" = \"$(id -un)\" ]; then" >> /root/.bashrc
    echo "    cd /home/$USER" >> /root/.bashrc
    echo "    su $USER" >> /root/.bashrc
    echo "fi" >> /root/.bashrc

    ## Setup Password-file
    PASSWDCONTENTS=$(grep -v "^${USER}:" /etc/passwd)
    GROUPCONTENTS=$(grep -v -e "^${GROUP}:" -e "^docker:" /etc/group)

    (echo "${PASSWDCONTENTS}" && echo "${USER}:x:$USER_ID:$GROUP_ID::/home/$USER:/bin/bash") > /etc/passwd
    (echo "${GROUPCONTENTS}" && echo "${GROUP}:x:${GROUP_ID}:") > /etc/group
    (if test -f /etc/sudoers ; then echo "${USER}  ALL=(ALL)   NOPASSWD: ALL" >> /etc/sudoers ; fi)
}

# main

# Create new user
## Check Inputs
echo -e "\033[1;33m ${USER_NAME} ${USER_ID} ${GROUP_NAME} ${GROUP_ID}\033[0m"
check_users

## Determine user & Setup Environment
if [ $CURRENT_USER_OK == true ]; then
    echo "  -->DOCKER_USER Input is set to '$USER_NAME:$USER_ID:$GROUP_NAME:$GROUP_ID'";
    echo -e "\033[0;32mSetting up environment for user=$USER_NAME\033[0m"
    setup_user $USER_NAME $USER_ID $GROUP_NAME $GROUP_ID
#    echo  "source /home/$USER_NAME/share/catkin_ws/devel/setup.bash" >> /home/$USER/.bashrc
else
    echo "  -->DOCKER_USER* variables not set. You need to set all four! Using 'root'.";
    echo -e "\033[0;32mSetting up environment for user=root\033[0m"
    USER_NAME="root"
fi

# Change shell to zsh
chsh -s /usr/bin/bash $USER_NAME

if which "$1" > /dev/null 2>&1 ; then
	$EXEC "$@"
else
	echo $@ | $EXEC $SHELL -li
fi


