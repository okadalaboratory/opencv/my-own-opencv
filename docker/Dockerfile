# Copyright (c) 2022 Hiroyuki Okada
# This software is released under the MIT License.
# http://opensource.org/licenses/mit-license.php
FROM  nvidia/cudagl:11.3.1-devel-ubuntu20.04 AS base

LABEL maintainer="Hiroyuki Okada <hiroyuki.okada@okadanet.org>"
LABEL org.okadanet.vendor="Hiroyuki Okada" \
      org.okadanet.dept="TRCP" \
      org.okadanet.version="1.0.0" \
      org.okadanet.released="Feburary 7, 2023"

ARG UBUNTU="20.04"
ARG CUDA="11.3.1"
ARG OPENCV="4.5.2"
ARG CUDA_ARCH_BIN="5.2 5.3 6.0 6.1 6.2 7.0 7.2 7.5 8.0 8.6"
ARG CUDA_ARCH_PTX="8.6"
ARG CUDNN="ON"
ARG ARCH=""
ENV DEBIAN_FRONTEND=noninteractive

RUN apt update && DEBIAN_FRONTEND=noninteractive apt install -y --no-install-recommends wget unzip git ca-certificates && rm -rf /var/lib/apt/lists/*

WORKDIR /tmp
RUN wget --no-check-certificate https://github.com/opencv/opencv/archive/refs/tags/${OPENCV}.zip && unzip ${OPENCV}.zip && rm ${OPENCV}.zip
RUN wget --no-check-certificate https://github.com/opencv/opencv_contrib/archive/${OPENCV}.zip && unzip ${OPENCV}.zip && rm ${OPENCV}.zip

RUN apt update && DEBIAN_FRONTEND=noninteractive apt install -y --no-install-recommends \
      build-essential \
      vim \
      cmake \
      ccache \
      ninja-build \
      python3 \
      python3-numpy \
      python3-setuptools \
      python3-pip \
      python3-dev \
      zlib1g-dev \
      libjpeg-dev \
      libpng-dev \
      libtiff5-dev \
      libopenjp2-7-dev \
      libgtk-3-dev \
      libavcodec-dev \
      libavformat-dev \
      libswscale-dev \
      libswresample-dev \
      ant \
      ant-optional \
      default-jdk \
      hdf5-tools \
      libhdf5-dev \
      liblept5 \
      tesseract-ocr \
      tesseract-ocr-eng \
      libtesseract-dev \
      libtesseract4 \
      libgdal-dev \
      libvtk7-dev \
      libgoogle-glog-dev \
      libatlas-base-dev \
      libeigen3-dev \
      libsuitesparse-dev \
      liblapacke-dev \
      libceres-dev \
      libeigen3-dev \
      libgtkglext1-dev \
      libopenni2-dev \
      libglfw3-dev \
      xvfb \
      pkg-config \
      libgstreamer1.0-dev \
      libgstreamer-plugins-base1.0-dev \
      gstreamer1.0-plugins-good \
      gstreamer1.0-plugins-ugly \
      gstreamer1.0-libav \
      libopenni2-dev \
      libdc1394-dev \
      libgphoto2-dev \
      ffmpeg ninja-build \
      language-pack-ja-base language-pack-ja locales fonts-takao \
    && rm -rf /var/lib/apt/lists/*

RUN pip install opencv-python

RUN mkdir opencv-${OPENCV}/build && \
    cd opencv-${OPENCV}/build && \
    cmake -GNinja -DOPENCV_EXTRA_MODULES_PATH=/tmp/opencv_contrib-${OPENCV}/modules \
      -DWITH_CUDA=ON \
      -DWITH_CUDNN=${CUDNN} \
      -DENABLE_FAST_MATH=ON \
      -DCUDA_FAST_MATH=ON \
      -DCUDA_ARCH_BIN=${CUDA_ARCH_BIN} \
      -DCUDA_ARCH_PTX=${CUDA_ARCH_PTX} \
      -DWITH_CUBLAS=ON \
      -DOPENCV_ENABLE_NONFREE=ON \
      -DCMAKE_BUILD_TYPE=RELEASE \
      -DCMAKE_INSTALL_PREFIX=/usr/local \
      -DBUILD_opencv_apps=ON \
      -DBUILD_opencv_python3=ON \
      .. && \
    ninja && \
    ninja install && \
    ldconfig && \
    rm -rf /tmp/*

# setup environment
ENV TZ Asia/Tokyo
# Locale
RUN locale-gen ja_JP.UTF-8
ENV LANG ja_JP.UTF-8
ENV LANGUAGE ja_JP:ja  
ENV LC_ALL ja_JP.UTF-8

RUN rm -rf /var/lib/apt/lists/*
CMD ["bash"]

FROM base AS user
RUN apt update && DEBIAN_FRONTEND=noninteractive apt install -y --no-install-recommends \
      x11-utils x11-apps terminator xterm  \
      && rm -rf /var/lib/apt/lists/*

# Install tmux 3.2
RUN apt-get update && apt-get install -y automake autoconf pkg-config libevent-dev libncurses5-dev bison && \
apt-get clean && rm -rf /var/lib/apt/lists/*
RUN git clone https://github.com/tmux/tmux.git && \
cd tmux && git checkout tags/3.2 && ls -la && sh autogen.sh && ./configure && make -j8 && make install

# Terminator Config
RUN mkdir -p /root/.config/terminator/
COPY ./docker/terminator_config /root/.config/terminator/config

# Remove display warnings
RUN mkdir /tmp/runtime-root
ENV XDG_RUNTIME_DIR "/tmp/runtime-root"
ENV NO_AT_BRIDGE 1

# Set up the work directory and entrypoint
COPY ./docker/ros_entrypoint.sh /
RUN chmod +x /ros_entrypoint.sh
ENTRYPOINT [ "/ros_entrypoint.sh" ]
CMD ["terminator"]


FROM base AS ros
## Install ROS CORE
# setup timezone
RUN echo 'Asia/Tokyo' > /etc/timezone; ln -s /usr/share/zoneinfo/Asia/Tokyo /etc/localtime; exit 0
RUN apt-get update && apt-get install -q -y --no-install-recommends tzdata

# install packages
RUN apt-get update && apt-get install -q -y --no-install-recommends dirmngr gnupg2

# setup keys
RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654

# setup sources.list
RUN echo "deb http://packages.ros.org/ros/ubuntu focal main" > /etc/apt/sources.list.d/ros1-latest.list

# setup environment
ENV TZ Asia/Tokyo
# Locale
RUN locale-gen ja_JP.UTF-8
ENV LANG ja_JP.UTF-8
ENV LANGUAGE ja_JP:ja  
ENV LC_ALL ja_JP.UTF-8

ENV ROS_DISTRO noetic
# install ros packages
RUN apt-get update && apt-get install -y --no-install-recommends ros-noetic-ros-core=1.5.0-1*

## ROS BASE
# install bootstrap tools
RUN apt-get update && apt-get install --no-install-recommends -y build-essential python3-rosdep python3-rosinstall python3-vcstools

# bootstrap rosdep
RUN rosdep init && rosdep update --rosdistro $ROS_DISTRO

# install ros packages
RUN apt-get update && apt-get install -y --no-install-recommends ros-noetic-ros-base=1.5.0-1*

## ROS PERCEPTION
# install ros packages
RUN apt-get update && apt-get install -y --no-install-recommends ros-noetic-perception=1.5.0-1*

RUN apt remove -y ros-noetic-vision-opencv
RUN apt -y install ros-noetic-cv-bridge

#COPY ros_entrypoint.sh /
#RUN chmod +x /ros_entrypoint.sh

RUN rm -rf /var/lib/apt/lists/*

#ENTRYPOINT ["/ros_entrypoint.sh"]
CMD ["bash"]


FROM ros AS ros-user
RUN apt update && DEBIAN_FRONTEND=noninteractive apt install -y --no-install-recommends \
      x11-utils x11-apps terminator xterm  \
      && rm -rf /var/lib/apt/lists/*

# Install tmux 3.2
RUN apt-get update && apt-get install -y automake autoconf pkg-config libevent-dev libncurses5-dev bison && \
apt-get clean && rm -rf /var/lib/apt/lists/*
RUN git clone https://github.com/tmux/tmux.git && \
cd tmux && git checkout tags/3.2 && ls -la && sh autogen.sh && ./configure && make -j8 && make install

# Terminator Config
RUN mkdir -p /root/.config/terminator/
COPY ./docker/terminator_config /root/.config/terminator/config

# Remove display warnings
RUN mkdir /tmp/runtime-root
ENV XDG_RUNTIME_DIR "/tmp/runtime-root"
ENV NO_AT_BRIDGE 1

# Set up the work directory and entrypoint
COPY ./docker/ros_entrypoint.sh /
RUN chmod +x /ros_entrypoint.sh
ENTRYPOINT [ "/ros_entrypoint.sh" ]
CMD ["terminator"]

