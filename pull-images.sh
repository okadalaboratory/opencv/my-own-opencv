#! /bin/bash
# Copyright (c) 2023 Hiroyuki Okada
# This software is released under the MIT License.
# http://opensource.org/licenses/mit-license.php

DOCKER_ID="okdhryk"

docker pull $DOCKER_ID/opencv:cudagl
docker pull $DOCKER_ID/opencv-user:cudagl
docker pull $DOCKER_ID/opencv-ros:cudagl
docker pull $DOCKER_ID/opencv-ros-user:cudagl

